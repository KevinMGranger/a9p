from setuptools import setup

setup(
    name="ninep",
    version="0.0.1",
    description="Tools to use the 9p protocol and make 9p-like protocols.",
    author="Kevin M Granger",
    packages=("ninep",),
    setup_requires=["pytest-runner"],
    tests_require=["pytest"]
)
