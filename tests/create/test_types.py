import pytest
import io
from ninep import create
from ninep.create import u8, u16, u32, u64, String

u8_nums = [
    (u8, 0, b'\x00'),
    (u8, 1, b'\x01'),
    (u8, 255, b'\xff')
]

u16_nums = [(u16, x, y + b'\x00') for _, x, y in u8_nums]
u16_nums.append((u16, (2**16) - 1, b'\xff\xff'))

u32_nums = [(u32, x, y + b'\x00\x00') for _, x, y in u16_nums]
u32_nums.append((u32, (2**32) - 1, b'\xff\xff\xff\xff'))

all_nums = u8_nums + u16_nums + u32_nums


@pytest.fixture(params=all_nums)
def parseable_int(request):
    typ, num, byt = request.param
    return (typ, io.BytesIO(byt), num)


def test_valid_ints_parse(parseable_int):
    num_type, buf, number = parseable_int
    assert num_type.from_bytes(buf) == number


@pytest.mark.parametrize("num_type,num,byts", all_nums)
def test_valid_ints_serialize(num_type, num, byts):
    buf = io.BytesIO(b"\x00" * len(byts))
    num_type(num).to_bytes(buf)
    assert buf.getvalue() == byts


@pytest.mark.parametrize("num_type,value",
                         [(u8, 2**8), (u16, 2**16), (u32, 2**32), (u64, 2**64)])
def test_too_large_nums(num_type, value):
    with pytest.raises(OverflowError):
        num_type(value)


@pytest.mark.parametrize("num_type", [u8, u16, u32, u64])
def test_no_negatives_unsigned(num_type):
    with pytest.raises(OverflowError):
        num_type(-1)


strings = [("testing", b'\x07\x00testing')]


@pytest.fixture(params=strings)
def valid_string(request):
    string, bytes = request.param
    return string, io.BytesIO(bytes)


def test_valid_strings(valid_string):
    string, buf = valid_string
    assert string == String.from_bytes(buf)

def test_valid_strings_serialize(valid_string):
    string, expected_buf = valid_string
    actual_buf = io.BytesIO(b'\x00' * (len(string) + 2))

    String(string).to_bytes(actual_buf)

    assert expected_buf.getbuffer() == actual_buf.getbuffer()

def test_invalid_string_with_nulls():
    buf = io.BytesIO(b"\x01\x00\x00")
    with pytest.raises(ValueError):
        string = String.from_bytes(buf)


def test_string_too_long():
    with pytest.raises(OverflowError):
        String("1" * 2**16)
