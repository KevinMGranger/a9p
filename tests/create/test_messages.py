import pytest
import io
from ninep.create import *

class MessageId(Message):
    msg_id = Field(u8)

class SimpleMessage(MessageId):
    msg = String

    def __init__(self, msg_id, msg):
        self.msg_id = msg_id
        self.msg = msg

msgs = [
    (0, "testing", b"\x00\x07\x00testing")
]

@pytest.fixture(params=msgs)
def messages(request):
    id, msg, bytes = request.param
    return (SimpleMessage(id, msg), bytes)

def test_message_serializes(messages):
    message, bytes = messages
    buf = io.BytesIO(b"\x00" * 10)

    message.to_bytes(buf)

    assert buf.getvalue() == bytes

def test_message_reads(messages):
    message, bytes = messages
    bytes = io.BytesIO(bytes)

    actual_message = SimpleMessage.from_bytes(bytes)

    assert message.msg_id == actual_message.msg_id
    assert message.msg == actual_message.msg

def test_message_stringifies(messages):
    message = SimpleMessage(0, "testing")

    assert str(message) == "SimpleMessage msg_id[0] msg[testing]"
