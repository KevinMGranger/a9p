import asyncio

class Server:
    def __init__(self, protoset, rx, wx):
        self.protoset = protoset
        self.rx = rx
        self.wx = wx

    async def serve(self):
        while True:
            msg = await self.protoset.read_msg(self.rx)
            handler = getattr(self, msg.__name__, None)
            if handler is None:
                raise Exception # TODO

            try:
                result = handler(msg)
                if asyncio.iscoroutine(result):
                    result = await result
            except Exception:
                # TODO: DO THE THING
                pass
