"""
9p for asyncio.
"""
import asyncio
import struct
import enum
import logging
# from . import py9p

_logger = logging.getLogger(__name__)

MSIZE_MAX = 8192
@enum.unique
class NineP2000MessageType(enum.IntEnum):
    Tversion = 100
    Rversion = 101
    Tauth = 102
    Rauth = 103
    Tattach = 104
    Rattach = 105
    # Terror = 106
    Rerror = 107
    Tflush = 108
    Rflush = 109
    Twalk = 110
    Rwalk = 111
    Topen = 112
    Ropen = 113
    Tcreate = 114
    Rcreate = 115
    Tread = 116
    Rread = 117
    Twrite = 118
    Rwrite = 119
    Tclunk = 120
    Rclunk = 121
    Tremove = 122
    Rremove = 123
    Tstat = 124
    Rstat = 125
    Twstat = 126
    Rwstat = 127

T_MESSAGES = {x for x in NineP2000MessageType if x.name.startswith('T')}
R_MESSAGES = {x for x in NineP2000MessageType if x.name.startswith('R')}

_msg_types = dict()



class NinePException(Exception):
    def __init__(self, msg, cause=None):
        self.msg = msg
        self.cause = cause


class Qid:
    _struct = struct.Struct("<BIQ")

    def __init__(self, qtype, version, path):
        self.qtype = qtype
        self.version = version
        self.path = path

    @classmethod
    def _from_bytes(cls, buf, offset):
        return cls(*cls._struct.unpack_from(buf, offset))

    def _to_bytes(self, buf, offset):
        self._struct.pack_into(buf, offset, self.qtype, self.version, self.path)


class Fcall:
    _struct = struct.Struct("<H")

    def __init__(self, tag):
        self.tag = tag

    def __len__(self):
        return self._struct.size

    def __repr__(self):
        return "size[{}] {} tag[{}] ".format(
            len(self), type(self).name, self.tag)



class Version(Fcall):
    _struct = struct.Struct("<IH")

    def __init__(self, msize, version, *, tag=None):
        super().__init__(tag)
        self.msize = msize
        self.version = version

    def __len__(self):
        return 7 + 4 + 2 + len(self.version.encode())

    def __repr__(self):
        return ''.join((super().__repr__(),
                        "msize[{}] version[{}]".format(
                            self.msize, self.version)))

    @classmethod
    def _from_bytes(cls, tag, rest):
        msize, strlen = Version._struct.unpack_from(rest)
        # TODO: check for proper len
        version = rest[6:6+strlen].decode()
        return cls(msize, version, tag=tag)

    def _to_bytes(self):
        yield from super()._to_bytes()
        yield from Version._struct.pack(self.msize, len(self.version))
        yield self.version.encode()


class Tauth(Fcall, Tmessage):
    def __init__(self, afid, uname, aname, *, tag=None):
        super().__init__(tag=tag)
        self.afid = afid
        self.uname = uname
        self.aname = aname

class Rerror(Fcall):
    def __init__(self, ename, *, tag=None):
        super().__init__(tag=tag)
        self.ename = ename

    def __len__(self):
        return 7 + 2 + len(self.ename)

    def __repr__(self):
        return "size[{}] Rerror tag[{}] ename[{}]".format(
            len(self), self.tag, self.ename)

    @staticmethod
    def _from_bytes(tag, rest):
        enamelen = u16.unpack_from(rest)
        # TODO: strlen
        ename = str(rest[2:2+enamelen])
        return Rerror(tag=tag, ename=ename)

    def _to_bytes(self):
        yield from super()._to_bytes()
        yield from _pack_str(self.ename)


class Tflush(Fcall, Tmessage):
    def __init__(self, oldtag, *, tag=None):
        super().__init__(tag)
        self.oldtag = oldtag

    def __len__(self):
        return 7 + 2

    def __repr__(self):
        return ''.join(
            (repr(super()), "oldtag[{}]".format(self.oldtag)))


class Rflush(Fcall):
    pass


class ConnectionFatalError(Rerror, NinePException):
    def __init__(self, ename, cause=None):
        Rerror.__init__(self, ename)
        NinePException.__init__(self, ename, cause)


class NinePProtocol(asyncio.StreamReaderProtocol):
    def __init__(self, limit=None, loop=None, read_time_limit=None):
        self.read_time_limit = read_time_limit
        self.transport = None
        loop = loop if loop else asyncio.get_event_loop()
        self.loop = loop

        # the documentation lies, default limit IS NOT NONE
        kwargs = dict(loop=loop)
        if limit:
            kwargs['limit'] = limit

        super().__init__(asyncio.StreamReader(**kwargs), self._connected)

    def _connected(self, reader, writer):
        self.reader = reader
        self.writer = writer
        self.transport = writer.transport
        peer = self.transport.get_extra_info('peername')
        if peer is not None:
            _logger.info("New connection to {}".format(peer))
        self.loop.create_task(self._read_loop())

    @asyncio.coroutine
    def _read_size_type(self):
        "Separated so you can timeout based on message type"
        _logger.debug("Reading msize")
        msize = yield from self.reader.readexactly(4)
        (msize,) = u32.unpack(msize)
        _logger.debug("Reading mtype")
        mtype = yield from self.reader.readexactly(1)
        (mtype,) = u8.unpack(mtype)
        return msize, mtype

    @asyncio.coroutine
    def _read_msg(self):
        msize, mtype = yield from self._read_size_type()
        _logger.debug("Reading tag")
        tag = yield from self.reader.readexactly(2)
        (tag,) = u16.unpack(tag)
        _logger.debug("Reading rest")
        rest = yield from self.reader.readexactly(msize-7)
        return msize, mtype, tag, rest

    @asyncio.coroutine
    def _read_loop(self):
        while True:
            try:
                _, mtype, tag, rest = yield from asyncio.wait_for(
                    self._read_msg(), self.read_time_limit)
                parser = _msg_types[NineP2000MessageType(mtype)]._from_bytes

                msg = parser(tag, rest)
                _logger.info("<-- {}".format(repr(msg)))
            except asyncio.IncompleteReadError:
                # stream closed TODO
                raise
            except asyncio.TimeoutError:
                # TODO: let user override how to handle
                raise
            except KeyError:
                # TODO missing parser (wtf?)
                raise
            except Exception:  # TODO: parser erro
                raise

            _logger.debug("Dispatching")
            if asyncio.iscoroutinefunction(self.dispatch):
                self.loop.create_task(self.dispatch(msg))
            else:
                self.loop.call_soon(self.dispatch, msg)


class NinePServerProtocol(NinePProtocol):
    def __init__(self, msize=MSIZE_MAX, version="9P2000", *args, **kwargs):
        # TODO: handle msize and limit
        super().__init__(*args, **kwargs)
        self.max_msize = msize
        self.msize = None
        self.version = version
        self.messages = dict()  # msg, task
        self.last_received = None
        self.last_sent = None

    @asyncio.coroutine
    def dispatch(self, msg):
        """
        Dispatch to the right handler function and send / handle the result.
        """
        try:
            if msg.tag in self.messages:
                # TODO: invalid tag?
                pass
            self.messages[msg.tag] = (msg, None)
            handler = getattr(self, msg.type.name.lower())

            _logger.debug("Handling")
            res = handler(msg)

            if asyncio.iscoroutine(res):
                self.messages[msg.tag] = (msg, res)
                res = yield from res

            if type(res) == tuple:
                res, drain = res
            else:
                drain = False

            if res is not None:
                if drain:
                    yield from self.send_drain(res)
                else:
                    self.send(res)
        except asyncio.CancelledError:
            # TODO: let this bubble up?
            raise
        except AttributeError:
            # TODO: handle missing method
            raise
        # TODO: something for user errors?

    def send(self, msg):
        self.last_sent = msg
        _logger.debug("--> {}".format(repr(msg)))
        self.writer.writelines(msg._to_bytes())

    def send_drain(self, msg):
        self.last_sent = msg
        _logger.debug("--> {}".format(repr(msg)))
        self.writer.writelines(msg._to_bytes())
        return self.writer.drain()

    def tversion(self, msg):
        if msg.version != self.version:
            raise ConnectionFatalError("unsupported version")

        self.msize = min(msg.msize, self.max_msize)

        return msg.respond(self.msize, self.version), True

    def tflush(self, msg):
        if msg.tag in self.messages:
            _, task = self.messages[msg.tag]
            task.cancel()
        return msg.respond()


class NinePClient(NinePProtocol):
    def __init__(self, msize=MSIZE_MAX, version="9p2000", on_connect=None,
                 *args, **kwargs):
        self.on_connect = on_connect
        self.msize = msize
        self.version = version

    @asyncio.coroutine
    def _connected(self, reader, writer):
        super()._connected(reader, writer)
        if self.on_connect is not None:
            res = self.on_connect()
            if asyncio.iscoroutine(res):
                self.loop.create_task(res)

    def dispatch(self, msg):
        """
        Find the correct waiting future for the message's given tag.
        If it is not found, call the provided error handler.
        """
        pass
