"""
"Types" in this module have the following common attributes:

    size: the instance's size, in bytes.
    max_size: the maximum size it can be, in bytes.
    bytelen: the static size of the type, or a symbol representing
        its dynamic size (like 's' for a string)

Numeric types have the following common attributes:
    MAX: the maximum value
    MIN: the minimum value
"""

import sys
import struct
import collections
import types
import inspect
import enum


def subclass_polyfill(cls, **kwargs):
    init = getattr(cls, '__init_subclass__', None)
    if init is not None:
        init(**kwargs)

    return cls

_pre36 = sys.version_info < (3, 6, 0)

# taken from http://bugs.python.org/file43835/pep487.patch
if _pre36:
    # the following code is a Python equivalent of the
    # C changes made for PEP 487.
    class type(type):
        @staticmethod
        def __prepare__(name, bases):
            return collections.OrderedDict()

        def __new__(cls, *args, **kwargs):
            if len(args) != 3:
                return super().__new__(cls, *args)
            name, bases, ns = args
            init = ns.get('__init_subclass__')
            if isinstance(init, types.FunctionType):
                ns['__init_subclass__'] = classmethod(init)
            self = super().__new__(cls, name, bases, ns)
            for k, v in self.__dict__.items():
                func = getattr(v, '__set_name__', None)
                if func is not None:
                    func(self, k)
            init = getattr(super(self, self), '__init_subclass__', None)
            if init is not None:
                init(**kwargs)
            return self
        def __init__(self, *args, **kwargs):
            pass


    class object:
        @classmethod
        def __init_subclass__(cls):
            pass

    class object(object, metaclass=type):
        pass

class Numeric(int):
    def __init__(self, x, base=None):
        if self < self.MIN:
            raise OverflowError(
                "{} is under the minimum of {} for type {}".format(
                    int(self), self.MIN, type(self)))
        if self > self.MAX:
            raise OverflowError(
                "{} is over the maximum of {} for type {}".format(
                    int(self), self.MAX, type(self)))

    @property
    def size(self):
        return self.bytelen

    @property
    def max_size(self):
        return self.bytelen

    @classmethod
    def from_bytes(cls, buf):
        num = cls.struct.unpack_from(buf.getbuffer(), buf.tell())[0]
        self = cls(num)
        buf.seek(cls.bytelen, 1)
        return self

    def to_bytes(self, buf):
        self.struct.pack_into(buf.getbuffer(), buf.tell(), self)
        buf.seek(self.bytelen, 1)

    def __str__(self):
        return "{}[{}]".format(self.__class__.__name__, int.__str__(self))

    # numeric magic methods below here
    # TODO: make parent wrap thing

class UnsignedInt(Numeric):
    @classmethod
    def __init_subclass__(cls):
        bytelen = cls.struct.size
        cls.bytelen = bytelen
        cls.MAX = (2**(bytelen*8)) - 1
        cls.MIN = 0


class u8(UnsignedInt):
    struct = struct.Struct("<B")


class u16(UnsignedInt):
    struct = struct.Struct("<H")


class u32(UnsignedInt):
    struct = struct.Struct("<I")


class u64(UnsignedInt):
    struct = struct.Struct("<Q")


if _pre36:
    for i in (u8, u16, u32, u64):
        subclass_polyfill(i)


class String(str):
    bytelen = 's'
    size_type = u16

    def __new__(cls, *args, **kwargs):
        self = super().__new__(cls, *args, **kwargs)
        if '\x00' in self:
            raise ValueError("No NULLs allowed")
        if len(self) > cls.size_type.MAX:
            raise OverflowError("String too long")

        return self

    @property
    def size(self):
        return self.size_type.size + len(self)

    @property
    def max_size(self):
        return self.size_type.MAX + self.size_type.size

    @classmethod
    def from_bytes(cls, buf):
        strlen = cls.size_type.from_bytes(buf)
        # TODO: test zero length
        return cls(buf.read(strlen), encoding="utf-8")

    def to_bytes(self, buf):
        self.size_type(len(self)).to_bytes(buf)
        buf.write(self.encode())

    def __str__(self):
        return "{}[{}]".format(self.__class__.__name__, str.__str__(self))


class Field:
    "A field descriptor"
    def __init__(self, inner_type):
        self.inner_type = inner_type
        self.name = None
        self.inner_name = None

    def __set_name__(self, owner, name):
        # TODO: save these somewhere? So they're not duplicated?
        # Or should people just have to make the classes themselves?
        if self.inner_type.__name__ != name:
            self.inner_type = type(name, (self.inner_type,), {})
        self.name = name
        self.inner_name = '_' + name

    def __get__(self, instance, cls=None):
        if instance is None:
            return self.inner_type
        else:
            return getattr(instance, self.inner_name)

    def __set__(self, instance, value):
        inner_type = self.inner_type
        if not isinstance(value, inner_type):
            value = inner_type(value)

        setattr(instance, self.inner_name, value)


def _typelike(t):
    "Tests if a type has the necessary attributes"
    return all(hasattr(t, attr) for attr in 'from_bytes to_bytes bytelen size'.split())

class MessageMeta(type):
    # TODO: custom dict that warns / errors on repeated names
    def __new__(mcs, name, bases, attrs, **kwargs):
        fields = collections.OrderedDict()
        for k, v in attrs.items():
            if isinstance(v, Field):
                fields[k] = v
            elif _typelike(v):
                v = Field(v)
                attrs[k] = v
                fields[k] = v

        attrs['_fields'] = fields
        attrs['_all_fields'] = None  # will be done in init

        return super().__new__(mcs, name, bases, dict(attrs))

    def __init__(cls, name, bases, attrs, **kwargs):
        class_dicts = (cl.__dict__ for cl in reversed(inspect.getmro(cls)))
        field_sets = (d['_fields'].values() for d in class_dicts if '_fields' in d)
        cls._all_fields = collections.OrderedDict((field.name, field)
                                                  for fields in field_sets
                                                  for field in fields)
        super().__init__(name, bases, attrs, **kwargs)

    def from_bytes(cls, buf):
        # oh my god I love python look at this
        kwargs = {name: field.inner_type.from_bytes(buf)
                  for name, field in cls._all_fields.items()}
        return cls(**kwargs)


class Message(metaclass=MessageMeta):
    @property
    def _members(self):
        yield from (getattr(self, name) for name in self._all_fields.keys())

    @property
    def size(self):
        return sum(field.size for field in self._members)

    def to_bytes(self, buf):
        for field in self._members:
            field.to_bytes(buf)

    def __str__(self):
        return self.__class__.__name__ + ' ' + ' '.join(str(x) for x in self._members)
